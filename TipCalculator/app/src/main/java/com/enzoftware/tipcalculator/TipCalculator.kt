package com.enzoftware.tipcalculator


/**
 * Created by Enzo Lizama Paredes on 6/4/20.
 * Contact: lizama.enzo@gmail.com
 */
data class TipCalculator(
    var bill: Double,
    var tipPercent: Double,
    var tipAmount: Double,
    var totalAmountValue: Double
) {
    fun calculateTipAmount() {
        tipAmount = bill * tipPercent
    }

    fun calculateAmountValue() {
        totalAmountValue = bill + tipAmount
    }
}