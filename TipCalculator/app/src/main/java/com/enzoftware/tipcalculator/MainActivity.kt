package com.enzoftware.tipcalculator

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var tipCalculator: TipCalculator
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initUi()
    }

    private fun initUi() {
        tipCalculator =
            TipCalculator(bill = 0.0, tipPercent = 0.15, tipAmount = 0.0, totalAmountValue = 0.0)
        bill_edit_text.addTextChangedListener {
            tipCalculator.bill = it.toString().toDouble()
            calculateTipValues()
        }

        tip_edit_text.addTextChangedListener {
            tipCalculator.tipPercent = it.toString().toDouble() / 100
            calculateTipValues()
        }
    }

    private fun calculateTipValues() {
        tipCalculator.calculateTipAmount()
        tipCalculator.calculateAmountValue()
        recalculateTip()
        recalculateTotalAmount()
    }

    private fun recalculateTip() {
        tip_value_text_view.text = tipCalculator.tipAmount.toString()
    }

    private fun recalculateTotalAmount() {
        amount_value_text_view.text = tipCalculator.totalAmountValue.toString()
    }

}